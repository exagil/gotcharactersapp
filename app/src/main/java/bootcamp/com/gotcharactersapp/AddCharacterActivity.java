package bootcamp.com.gotcharactersapp;

import android.app.AlertDialog;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class AddCharacterActivity extends AppCompatActivity {

    private static final int IMAGE_REQUEST_CODE = 1;
    private static final String LOG_TAG = "chirag";
    private static final int NO_HOUSE_SELECTED_ID = -1;
    private ImageView mNewCharacterImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_character);

        mNewCharacterImage = (ImageView) findViewById(R.id.new_character_image);
        mNewCharacterImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(intent, IMAGE_REQUEST_CODE);
            }
        });

        final EditText newCharacterName = (EditText) findViewById(R.id.new_character_name);
        final RadioGroup allHouses = (RadioGroup) findViewById(R.id.new_character_house);
        Button saveButton = (Button) findViewById(R.id.save_new_character);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedHouseId = allHouses.getCheckedRadioButtonId();

                if (newCharacterName.getText().length() == 0) {
                    newCharacterName.setError("ERRRROR");
                }

                if(selectedHouseId == NO_HOUSE_SELECTED_ID) {
                    showElementNotSelectedAlertDialogFor("House");
                    return;
                }

                if(mNewCharacterImage.getTag() == null) {
                    showElementNotSelectedAlertDialogFor("Image");
                    return;
                }

                DBHelper dbHelper = DBHelper.getInstance(getApplicationContext());
                RadioButton selectedHouse = (RadioButton) findViewById(selectedHouseId);
                final String selectedHouseName = selectedHouse.getTag().toString();

                GotCharacter gotCharacter = new GotCharacter(newCharacterName.getText().toString(), mNewCharacterImage.getTag().toString(),
                        mNewCharacterImage.getTag().toString(), true, selectedHouseName, R.drawable.stark, "Lorem Ipsum");
                dbHelper.insert(gotCharacter);
                Toast toast = Toast.makeText(getApplicationContext(), "Character Saved!", Toast.LENGTH_SHORT);
                toast.show();
                finish();
                Log.d(LOG_TAG, selectedHouseName);
            }

            private void showElementNotSelectedAlertDialogFor(String elementName) {
                AlertDialog.Builder elementNotSelectedAlertBuilder = new AlertDialog.Builder(AddCharacterActivity.this);
                elementNotSelectedAlertBuilder.setMessage("No " + elementName + "Selected")
                        .setTitle("Error");
                AlertDialog elementNotSelectedAlert = elementNotSelectedAlertBuilder.create();
                elementNotSelectedAlert.show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(LOG_TAG, String.valueOf(requestCode));
        Log.d(LOG_TAG, String.valueOf(resultCode));
        Log.d(LOG_TAG, String.valueOf(data));
        if (requestCode == IMAGE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
//                try {
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
//                    ImageView imageView = (ImageView) findViewById(R.id.new_character_image);
//                    imageView.setImageBitmap(bitmap);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                Uri contentUri = data.getData();
//                Uri uri = data.getData();
                String[] projection = {MediaStore.Images.Media.DATA};
                Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
                cursor.moveToFirst();
                Log.d(LOG_TAG, DatabaseUtils.dumpCursorToString(cursor));
                int columnIndex = cursor.getColumnIndex(projection[0]);
                String picturePath = cursor.getString(columnIndex); // returns null
                Log.d(LOG_TAG, picturePath);

                cursor.close();
                mNewCharacterImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
                mNewCharacterImage.setTag("file://" + picturePath);
            }
        }
    }
}
