package bootcamp.com.gotcharactersapp;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

public class CharacterSilderActivity extends FragmentActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_slider);
        CharacterSliderAdapter characterSliderAdapter = new CharacterSliderAdapter(getSupportFragmentManager(), this);
        ViewPager viewPager = (ViewPager) findViewById(R.id.character_slider);
        viewPager.setAdapter(characterSliderAdapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class CharacterSliderAdapter extends FragmentStatePagerAdapter {

        private DBHelper dbHelper;
        private Cursor cursor;

        public CharacterSliderAdapter(FragmentManager fm, Context context) {
            super(fm);
            dbHelper = DBHelper.getInstance(context);
            cursor = dbHelper.getAllRows();
        }

        @Override
        public Fragment getItem(int position) {
            GotCharacter gotCharacter = getCharacterAt(position);


            DetailsFragment detailsFragment = new DetailsFragment();
            Bundle args = new Bundle();
            args.putParcelable(DetailsActivity.GOT_CHARACTER_KEY, gotCharacter);

            detailsFragment.setArguments(args);
            return detailsFragment;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getCharacterAt(position).name;
        }

        @NonNull
        private GotCharacter getCharacterAt(int position) {
            cursor.moveToPosition(position);

            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String thumbUrl = cursor.getString(2);
            String fullUrl = cursor.getString(3);
            String house = cursor.getString(4);
            int houseResId = cursor.getInt(5);
            String description = cursor.getString(6);
            return new GotCharacter(id, name, thumbUrl, fullUrl, false, house, houseResId, description);
        }

        @Override
        public int getCount() {
            return cursor.getCount();
        }
    }
}
