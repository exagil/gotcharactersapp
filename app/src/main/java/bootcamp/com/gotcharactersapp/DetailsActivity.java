package bootcamp.com.gotcharactersapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends AppCompatActivity {
    public static String GOT_CHARACTER_KEY = "got_character";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        GotCharacter gotCharacter = getIntent().getParcelableExtra(GOT_CHARACTER_KEY);

        DetailsFragment detailsFragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(GOT_CHARACTER_KEY, gotCharacter);

        detailsFragment.setArguments(args);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.detail_container, detailsFragment)
                .commit();

    }
}
