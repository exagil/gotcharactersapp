package bootcamp.com.gotcharactersapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {

    static final String GOT_DB_NAME = "got";
    static final int VERSION = 1;
    private static DBHelper dbHelper;
    private Cursor allRows;

    private DBHelper(Context context) {
        super(context, GOT_DB_NAME, null, VERSION);
    }

    public static DBHelper getInstance(Context context) {
        if (dbHelper == null) {
            dbHelper = new DBHelper(context);
        }
        return dbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String createSql = "CREATE TABLE " + GotCharacter.GOT_TABLE + "(" +
                GotCharacter._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                GotCharacter.NAME + " TEXT," +
                GotCharacter.THUMB_URL + " TEXT," +
                GotCharacter.FULL_URL + " TEXT," +
                GotCharacter.HOUSE + " TEXT," +
                GotCharacter.HOUSE_RES_ID + " INTEGER," +
                GotCharacter.DESCRIPTION + " TEXT);";
        database.execSQL(createSql);
        for (GotCharacter gotCharacter : MainActivity.Got_CHARACTERS) {
            insert(database, gotCharacter);
        }
    }


    public void insert(GotCharacter gotCharacter) {
        insert(getWritableDatabase(), gotCharacter);
    }

    private void insert(SQLiteDatabase database, GotCharacter gotCharacter) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(GotCharacter.NAME, gotCharacter.name);
        contentValues.put(GotCharacter.THUMB_URL, gotCharacter.thumbUrl);
        contentValues.put(GotCharacter.FULL_URL, gotCharacter.fullUrl);
        contentValues.put(GotCharacter.HOUSE, gotCharacter.house);
        contentValues.put(GotCharacter.HOUSE_RES_ID, gotCharacter.houseResId);
        contentValues.put(GotCharacter.DESCRIPTION, gotCharacter.description);
        database.insert(GotCharacter.GOT_TABLE, null, contentValues);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public int getRowCount() {
        return (int) DatabaseUtils.longForQuery(getReadableDatabase(), "select count(*) from " + GotCharacter.GOT_TABLE, new String[0]);
    }


    public Cursor getAllRows() {
        SQLiteDatabase sqLiteDatabase = dbHelper.getReadableDatabase();
        return sqLiteDatabase.query(GotCharacter.GOT_TABLE, GotCharacter.ALL_COLUMNS, null, null, null, null, null);
    }
}
