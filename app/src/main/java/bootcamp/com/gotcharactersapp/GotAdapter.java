package bootcamp.com.gotcharactersapp;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class GotAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private Cursor cursor;
    Context context;

    public GotAdapter(Context context) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        DBHelper dbHelper = DBHelper.getInstance(context);
        cursor = dbHelper.getAllRows();
    }

    @Override
    public int getCount() {
        return cursor != null && !cursor.isClosed() ? cursor.getCount() : 0;
    }

    @Override
    public GotCharacter getItem(int position) {
        cursor.moveToPosition(position);

        int id = cursor.getInt(0);
        String name = cursor.getString(1);
        String thumbUrl = cursor.getString(2);
        String fullUrl = cursor.getString(3);
        String house = cursor.getString(4);
        int houseResId = cursor.getInt(5);
        String description = cursor.getString(6);
        GotCharacter gotCharacter = new GotCharacter(id, name, thumbUrl, fullUrl, false, house, houseResId, description);

        return gotCharacter;
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
            GotCharacterViewHolder gotCharacterViewHolder = new GotCharacterViewHolder(
                    (TextView) view.findViewById(R.id.item_name),
                    (ImageView) view.findViewById(R.id.got_image)
            );
            view.setTag(gotCharacterViewHolder);
        } else {
            view = convertView;
        }

        GotCharacter gotCharacter = getItem(position);
        GotCharacterViewHolder holder = (GotCharacterViewHolder) view.getTag();
        Picasso.with(context)
                .load(gotCharacter.thumbUrl)
                .placeholder(R.drawable.house_placeholder)
                .into(holder.imageView);
        holder.textView.setText(gotCharacter.name);

        return view;
    }

    public void onStart() {
        onStop();
        cursor = DBHelper.getInstance(context).getAllRows();
    }

    public void onStop() {
        if (cursor != null && !cursor.isClosed()) cursor.close();
    }
}
