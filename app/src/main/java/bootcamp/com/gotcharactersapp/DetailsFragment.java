package bootcamp.com.gotcharactersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsFragment extends Fragment {

    private TextView textName;
    private ImageView imageCharacter;
    private ImageView imageHouse;
    private TextView textDescription;
    private TextView textHouse;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);
        Bundle arguments = getArguments();

        textName = (TextView) view.findViewById(R.id.character_name);
        imageCharacter = (ImageView) view.findViewById(R.id.character_image);
        imageHouse = (ImageView) view.findViewById(R.id.character_house);
        textDescription = (TextView) view.findViewById(R.id.character_description);
        textHouse = (TextView) view.findViewById(R.id.house_name);

        GotCharacter gotCharacter = null;
        if (arguments != null)
            gotCharacter = arguments.getParcelable("got_character");

        if (gotCharacter != null) {
            populate(gotCharacter);
        } else {
            textDescription.setText("No Character Supplied");
        }
        return view;
    }

    public void populate(GotCharacter gotCharacter) {
        String name = gotCharacter.name;
        String fullUrl = gotCharacter.fullUrl;
        int houseResId = gotCharacter.houseResId;
        String house = gotCharacter.house;
        String description = gotCharacter.description;


        textName.setText(name);
        textDescription.setText(description);
        textHouse.setText(house);

        Picasso.with(getContext())
                .load(fullUrl)
                .placeholder(R.drawable.house_placeholder)
                .error(R.drawable.profile_placeholder_error)
                .into(imageCharacter);

        Picasso.with(getContext())
                .load(houseResId)
                .placeholder(R.drawable.house_placeholder)
                .into(imageHouse);
    }
}
