package bootcamp.com.gotcharactersapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

public class ListFragment extends Fragment {
    private static final String GOT_CHARACTER_KEY = "got_character";
    private GotListAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        final ListView list = (ListView) view.findViewById(R.id.listView);
        GotService goTService = ((GotApplication) getActivity().getApplication()).getGoTService();
        adapter = new GotListAdapter(getActivity(), goTService);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (adapter.getItemViewType(position) == 0) {
                    ((Callback) getActivity()).clicked(adapter.getItem(position));
                } else {
                    adapter.loadMore();
                }
            }
        });
        return view;
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        adapter.onStart();
//    }
//
//    @Override
//    public void onStop() {
//        super.onStop();
//        adapter.onStop();
//    }

    public interface Callback {
        void clicked(GotCharacter gotCharacter);
    }
}
