package bootcamp.com.gotcharactersapp;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface GotService {
    @GET("/got_characters/{id}.json")
    public GotCharacter getCharacter(@Path("id") Integer id);

    @GET("/got_characters.json")
    public List<GotCharacter> getCharacters(@Query("page") Integer page);

    @POST("/got_characters.json")
    public GotCharacter newCharacter(@Body GotCharacter goTCharacter);
}