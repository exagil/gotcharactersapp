package bootcamp.com.gotcharactersapp;

import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by chi6rag on 12/8/15.
 */
public class GotCharacterViewHolder {
    public final TextView textView;
    public final ImageView imageView;

    public GotCharacterViewHolder(TextView textView, ImageView imageView) {
        this.textView = textView;
        this.imageView = imageView;
    }
}
