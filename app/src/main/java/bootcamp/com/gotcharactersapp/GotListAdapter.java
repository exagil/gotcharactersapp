package bootcamp.com.gotcharactersapp;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class GotListAdapter extends BaseAdapter {
    private static final int GOT_CHARACTER_VIEW_TYPE = 1;
    private static final int LOAD_BUTTON_VIEW_TYPE = 0;

    private final ArrayList<GotCharacter> data;
    private Activity mContext;
    private final GotService mGoTService;
    private LayoutInflater mLayoutInflater;
    private int i;

    public GotListAdapter(Activity context, GotService goTService) {
        this.mContext = context;
        mGoTService = goTService;
        this.mLayoutInflater = context.getLayoutInflater();
        data = new ArrayList<>();

    }

    @Override
    public int getCount() {
        return data.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        return position == getCount() - 1 ? 1 : 0;
    }

    @Override
    public GotCharacter getItem(int position) {
        if (getItemViewType(position) == 0) return data.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (getItemViewType(position) == 0) {
            return getGoTView(position, convertView, parent);
        }
        View view;
        if (convertView == null)
            view = mLayoutInflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        else
            view = convertView;
        ((TextView) view).setText("Load More");
        return view;
    }

    private View getGoTView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = mLayoutInflater.inflate(R.layout.list_item, parent, false);
            GotCharacterViewHolder gotCharacterViewHolder = new GotCharacterViewHolder(
                    (TextView) view.findViewById(R.id.item_name),
                    (ImageView) view.findViewById(R.id.got_image)
            );
            view.setTag(gotCharacterViewHolder);
        } else {
            view = convertView;
        }

        GotCharacter gotCharacter = getItem(position);
        GotCharacterViewHolder holder = (GotCharacterViewHolder) view.getTag();
        Picasso.with(mContext)
                .load(gotCharacter.thumbUrl)
                .placeholder(R.drawable.house_placeholder)
                .into(holder.imageView);
        holder.textView.setText(gotCharacter.name);

        return view;
    }


    public void loadMore() {
        new AsyncTask<Integer, Void, List<GotCharacter>>(){
            @Override
            protected List<GotCharacter> doInBackground(Integer... params) {
                return mGoTService.getCharacters(params[0]);
            }

            @Override
            protected void onPostExecute(List<GotCharacter> gotCharacters) {
                data.addAll(gotCharacters);
                notifyDataSetChanged();
                i++;
            }
        }.execute(i + 1);
    }
}
